<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

    function __construct()
	{
		parent:: __construct();
       $this->load->model('Modelo_per');

		$this->load->helper(array('url','language'));
		
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('postu');
	}

	public function postulacion_jefe()
	{
		$this->load->view('postulaciones/jefe');
	}
	public function postulacion_subjefe()
	{

		$this->load->view('postulaciones/subjefe');
	}
	public function guardar_jefe()
	{

		$this->form_validation->set_rules('nombre', $this->lang->line('nombre'), 'required');
		$this->form_validation->set_rules('paterno', $this->lang->line('paterno'), 'required');
		$this->form_validation->set_rules('materno', $this->lang->line('materno'), 'required');
		$this->form_validation->set_rules('celular', $this->lang->line('celular'), 'required');
///////cargo de secretario
		if ($this->form_validation->run() == true) {
			
		$dato_persona=array(
			'nombre' => $this->input->post('nombre'),
			'paterno' => $this->input->post('paterno'),
			'materno' => $this->input->post('materno'),
			'celular' => $this->input->post('celular'),

			);
			//$this->Modelo_per->guardar_datos($dato_persona);
			echo "guardado";

		}else{
			echo "error";
		}

	}public function guardar_subjefe()
	{

		$this->form_validation->set_rules('nombre', $this->lang->line('nombre'), 'required');
		$this->form_validation->set_rules('paterno', $this->lang->line('paterno'), 'required');
		$this->form_validation->set_rules('materno', $this->lang->line('materno'), 'required');
		$this->form_validation->set_rules('celular', $this->lang->line('celular'), 'required');
///////cargo de secretario
		if ($this->form_validation->run() == true) {
			
		$dato_persona=array(
			'nombre' => $this->input->post('nombre'),
			'paterno' => $this->input->post('paterno'),
			'materno' => $this->input->post('materno'),
			'celular' => $this->input->post('celular'),

			);
			$this->Modelo_per->guardar_datos($dato_persona);
		}else{
			echo "error";
		}

	}

}
